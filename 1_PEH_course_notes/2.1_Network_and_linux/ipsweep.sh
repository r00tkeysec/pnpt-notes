#!/bin/bash
if [ "10.0.2" == "" ]
then
echo "You forgot an IP address!"

else
for ip in `seq 1 254` ; do
ping -c 1 10.0.2.$ip | grep "64 bytes" | cut -d " " -f 4 | tr -d ":"&
done
fi
