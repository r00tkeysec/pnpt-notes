#!/bin/python3

import sys
import socket
from datetime import datetime as dt

#Define our target 

#sys.argv means: "python3 15_port_scanner.py <192.168.*.*>"

if len(sys.argv) == 2:
	target = socket.gethostbyname(sys.argv[1]) #Translate hostname to IPv4
else:
	print("Invalid amount of arguments.")
	print("Syntax: python3 port_scanner.py <ip>")

#Add a pretty banner.
print("-" * 50)
print(f"Scanning target {target}")
print(f"Time started {dt.now()}")
print("-" * 50)

try:
	for port in range(1,1000):
		#sock = socket.socket (socket_family, socket_type)
		s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
		socket.setdefaulttimeout(0.1)
		result = s.connect_ex((target,port)) #returns an error indicator.
		if result == 0:
			print(f"Port {port} is open.")
		s.close()

except KeyboardInterrupt:
	print("\nExiting Porgram.")
	sys.exit()
except socket.gaierror:
	print("Host name could not be resolved.")
	sys.exit()
except socket.error:
	print("couldn't connect to server.")
	sys.exit()

