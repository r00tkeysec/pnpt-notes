#!/bin/python

#Vars and Methods.

quote = "You are definately going to make it through PNPT."
print(quote)

# Methods = functions that are available for a given object.

print(quote.upper()) #Uper Case.
print(quote.lower()) #Lower Case.
print(quote.title()) #Title case.
print(len(quote)) #Length

# Strip can only remove from beginning and ending. So in quote you can either remove "You" or "PNPT."
print(quote.strip("definately")) #we tried to remove form middle and no change happens.
var2 = 'You are stupid'
print(var2.strip('You'))

name = "Visvesh"
age = 22
gpa = 3.0

print(int(gpa))

print(f"My name is {name} and my age is {age} years old." + f" I have major in IN engineering with {gpa} gpa.")

age2 = 1 + age

print(age2)

