#!/bin/python3

def nl():
	print("\n")
	
#Advanced Strings

my_name = "Visvesh Kasodariya"
print(my_name[0])
print(my_name[-1])
print(len(my_name))
print(my_name[:7])
nl()
print(my_name.split())
nl()
name_split = my_name.split()
name_join = ' '.join(name_split)
print(name_join)
nl()

another_exp = "Please Do Not Throw Sausage Pizza Away"
exp_split = another_exp.split()
print(exp_split)
exp_join = ' '.join(exp_split)
print(exp_join)

nl()

quote = "He said, 'give me all your money'"
quote2 = "He said, \"give me all your money\""
print(quote)
print(quote2)

nl()

too_much_space = "     hello World!     "
print(too_much_space.strip())

nl()

print("a" in "Apple") #False
letter = "a"
word = "Apple"
print(letter.lower() in word.lower()) #True

movie = "The Hangover"
print("My favorite movie is {}.".format(movie))
print(f"My fav movie is {movie}.")
