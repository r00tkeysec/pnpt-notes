#!/bin/python3

# Math Operators.

print(50+50) #add
print(50-50) #sub
print(50*50) #mul
print(50**3) #exponential ** actualy means 50^3
print(50/50) #division.
print(50//50) #division without integer.
print(20%10) #modulo, in other word "remainder"
print(50-50+50/50*50) #PEMDAS

pemdas = (341323424-40232+123*12333**2323%22/123//12)
print("pemdas answer will be {}".format(pemdas))
print(f"additon: {20+30}")

