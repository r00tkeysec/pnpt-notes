> Author :r00tkey
> Date: 6/7/21

# What we'll Cover.

- [Strings](#strings)
- [Math](#math)
- [Variables and Methods](#vars)
- [Functions](#funcs)
- [Boolean Expressions](#bools)
- [Relational Operators](#relational)
- [Conditional Statements](#conditons)
- [Lists](#list)
- [Tuples](#tuples)
- [Looping](#loop)
- [Importing Modules](#importing)
- [Advanced Strings](#advanced-strings)
- [Dictionaries](#dicts)
- [Sockets](#socket)
- [Port Scanner](#tool-building)

--------------------------------------------------------------------

<a name="strings"/>

1. Strings
> [Click Here](1_strings.py)

--------------------------------------------------------------------
<a name="math"/>

2. Math 
> [Click Here](2_math.py)

--------------------------------------------------------------------
<a name="vars"/>

3. Variables
> [Click Here](3_vars_and_methods.py)

--------------------------------------------------------------------
<a name="funcs"/>

4. Functions
> [Click Here](4_functions.py)

--------------------------------------------------------------------
<a name="bools"/>

5. Boolean Expression
> [Click Here](5_boolexp.py)

--------------------------------------------------------------------
<a name="relational"/>

6. Relational and Booleans.
> [Click Here](6_relational_and_bool.py)

--------------------------------------------------------------------
<a name="conditons"/>

7. Conditional Statements
> [Click Here](7_conditional_stat.py)

--------------------------------------------------------------------
<a name="list"/>

8. Lists
> [Click Here](8_lists.py)

--------------------------------------------------------------------
<a name="tuples"/>

9. Tuples
> [Click Here](9_tuples.py)

--------------------------------------------------------------------
<a name="loop"/>

10. Looping
> [Click Here](10_looping.py)

--------------------------------------------------------------------
<a name="importing"/>

11. Importing Modules
> [Click Here](11_importing_modules.py)

--------------------------------------------------------------------
<a name="advanced-strings"/>

12. Advanced Strings 
> [Click Here](12_advanced_string.py)

--------------------------------------------------------------------
<a name="dicts"/>

13. Dictionaries 
> [Click Here](13_dictionaries.py)

--------------------------------------------------------------------
<a name="socket"/>

14. Sockets
> [Click Here](14_sockets.py)

--------------------------------------------------------------------
<a name="tool-building"/>

15. Port Scanner
> [Click Here](15_port_scanner.py)





