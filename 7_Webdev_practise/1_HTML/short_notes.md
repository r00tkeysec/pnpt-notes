# HTML Practice.

- [HTML Practice.](#html-practice)
- [Boilerplate](#boilerplate)
- [Some Tags.](#tags)

-----------------------------------------------------------------------------------------------

<a name="boilerplate"/>

## 1. Boilerplate

```html
<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<title> Hi This is Boiler plate</title>
	</head>
	<body>
		<p> Hi, Just a para </p>
	</body>
</html>

```

----------------------------------------------------------------------------------------------------;

<a name="tags"/>

## 2. Some Tags.

> [**Go check out "HTML Elements" on Dev Docs**](https://devdocs.io/html-elements/ "Dev-Docs Elements")
```html
h1 to h6 = <h1> text </h1> to <h6> text </h6>
hr = <hr> text </hr>
center = <center>text</center>
br (break) = <br>
em (italic) = <em></em> or <i></i>
strong (bold) = <strong></strong> or <b></b>
```
>Lists
```html
Two types of Lists ordered and Un-ordered Lists.

1. Un-ordered List.
------------------------------
<ul>
	<li>Milk</li>
	<li>Bread
		<ul>
			<li>Brown Bread</li>
		</ul>
	</li>
</ul>

2. Ordered List.
------------------------------
<ol>
	<li>Security Analysis</li>
	<li>Instruments
		<ul>
			<li>Laptop</li>
			<li>Cables</li>
		</ul>
	</li>
</ol>
```
> Anchor & Image tag.

```html
1. Anchor Tag.
--------------
<a target="_blank" href="https://www.google.com/">Google Website</a>

2. Image Tag.
-------------
<img alt="Google Logo" src="https://www.google.com/images/branding/googlelogo/2x/googlelogo_color_160x56dp.png">
```
> Tables.

```html
> Tables:
---------------

1. Head: <thead></thead>
2. th (table head): <th></th>
3. tr (tabel row), tr are always horizontal : <tr></tr>
4. td (table data a.k.a cells), td are always vertical : <td></td>

> So Let's look at very simple code.
---------------------------------------------------------
<table>
	<thead>
		<tr>
			<th>The Table Header</th>
		</tr>
	</thead>
	<tbody>
		<tr>
			<td>Data-1</td>
			<td>Data-2</td>
		</tr>
		<tr>
			<td>Data-3</td>
			<td>Data-4</td>
		</tr>
	</tbody>
</table>
```
`Above Table Code will look like` <br> ![Above Table Code will look like](images/table.JPG)

> Forms, lables & inputs

```html
form = <form action="" method="get" autocomplete="on" accept-charset="utf-8></form>
label = <label for="email">Enter Your email: </label> 
input = <input type="email" id="email">


<form method="post" name="">
	<label for="email" name="email">Enter Your Email: </label>
	<input type="month" name="month">
</form>

```
